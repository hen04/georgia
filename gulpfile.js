'use strict';

const gulp = require('gulp');
const plugins = require('gulp-load-plugins')();

const style = require('gulp-sass');

const clean = require('gulp-clean');

const svgCheerio = require('gulp-cheerio');
const svgReplace = require('gulp-replace');
const svgSprite = require('gulp-svg-sprite');
const svgMin = require('gulp-svgmin');

const pngquant = require('imagemin-pngquant');

const data = require('gulp-data');
const pathJson = require('gulp-path');
const fs = require('fs');

const rename = require('gulp-rename');

const newer  = require ('gulp-newer'); // обновления только новых картинок

const gcmq = require('gulp-group-css-media-queries');

const merge = require('merge-stream');

const runSequence = require('run-sequence'); // последовательная обработка build
const urlAdjuster = require('gulp-css-url-adjuster'); // подмена адреса у background для bitrix

const browserSync = require('browser-sync').create();

// Пути для сборки
var path = {
  build: {
    root: 'build/',
    html: 'build/',
    json: 'build/data/',
    js: 'build/js/',
    jslibs: 'build/js/libs/',
    css: 'build/css/',
    cssDsklad: '../../../../local/assets/css/',
    cssSprite: '../../../../local/assets/images/svg/',
    cssBasket: '../../../components/dsklad/basket/templates/.default/',
    cssCheckout: '../../../templates/order/components/bitrix/sale.order.ajax/show/',
    cssMain: '../../../templates/dsklad/css/index/',
    cssCatalog: '../../../templates/dsklad/css/catalog/',
    cssCatalogDetail: '../../../templates/dsklad/components/bitrix/catalog.element/catalogDsklad/',
    cssRegionPopup: '../../../templates/order/css/',
    cssPersonal: '../../../templates/dsklad/css/personal/',
    cssVoprosyOtvety: '../../../templates/dsklad/css/voprosy-otvety/',
    csslibs: 'build/css/libs/',
    maps: '/maps',
    images: 'build/images/',
    svg: './build/images/svg/'
  },
  src: {
    root: 'src/',
    pug: 'src/pug/**/*.pug',
    jsonDir: 'src/data/',
    json: 'src/data/**/*.json',
    js: 'src/js/**/*.js',
    jslibs: 'src/js/libs/',
    sass: 'src/sass/**/*.sass',
    cssDsklad: ['build/css/dsklad-styles.css', 'build/css/templates/purepopup.css'],
    cssSprite: 'build/images/svg/sprite.svg',
    cssBasket: 'build/css/basket/style.css',
    cssCheckout: 'build/css/checkout/style.css',
    cssMain: 'build/css/index/style.css',
    cssCatalog: 'build/css/catalog/style.css',
    cssCatalogDetail: 'build/css/catalog-detail/style.css',
    cssRegionPopup: 'build/css/templates/region-popup.css',
    cssPersonal: 'build/css/personal/style.css',
    cssVoprosyOtvety: 'build/css/voprosy-otvety/style.css',
    csslibs: 'src/sass/libs/*.*',
    images: 'src/images/*.*',
    svg: 'src/images/svg/*.svg',
    spriteStyle: 'sass/_sprite.scss',
    spriteTemplate: 'src/sass/templates/_sprite_template.scss',
  },
  watch: {
    root: './build'
  }
};

// pug
gulp.task('pug', function(){
  return gulp.src(path.src.pug)
    .pipe(data(function(file) {
      return JSON.parse(fs.readFileSync('./src/data/data.json'));
    }))
    .pipe(plugins.pug({pretty: true}))
    .on('error', console.log)
    .pipe(gulp.dest(path.build.html))
    .on('end', browserSync.reload);
});


gulp.task('json', function() {
  return gulp.src(path.src.json)
    .pipe(gulp.dest(path.build.json))
    .on('end', browserSync.reload);
});


// style
gulp.task('style', function () {
  return gulp.src(path.src.sass)
  //.pipe(changed(path.build.css))
    .pipe(plugins.sourcemaps.init({loadMaps: true}))
    .pipe(style({outputStyle: 'expanded'}).on('error', style.logError))
    //.pipe(plugins.autoprefixer({browserslist: ['last 2 versions']}))
    .pipe(gcmq())
    .pipe(plugins.sourcemaps.write(path.build.maps))
    .pipe(gulp.dest(path.build.css))
    .on('end', browserSync.reload);
});


// cssSprite
gulp.task('cssSprite', function () {
  return gulp.src(path.src.cssSprite) // тут пути
    .pipe(gulp.dest(path.build.cssSprite));
});

// cssRegionPopup
gulp.task('cssRegionPopup', function () {
  return gulp.src(path.src.cssRegionPopup) // тут пути
    .pipe(urlAdjuster({
      //replace: ['../../images/svgsvg/','../images/svg/']
    }))
    .pipe(gulp.dest(path.build.cssRegionPopup));
});

// csslibs
gulp.task('csslibs', function () {
  return gulp.src(path.src.csslibs) // тут пути
    .pipe(gulp.dest(path.build.csslibs));
});

// jslibs
gulp.task('jslibs', function () {
  return gulp.src(path.src.jslibs) // тут пути
    .pipe(gulp.dest(path.build.jslibs));
});

// fonts
gulp.task('fonts', function () {
  return gulp.src(path.src.fonts) // тут пути
    .pipe(gulp.dest(path.build.fonts));
});

// js
gulp.task('js', function () {
  return gulp.src(path.src.js)
    .pipe(gulp.dest(path.build.js))
    .on('end', browserSync.reload);
});

// imagemin
gulp.task('images', function () {
  return gulp.src(path.src.images)
    .pipe(newer(path.build.images))
    .pipe(plugins.imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(gulp.dest(path.build.images))
});


// svg-sprite
gulp.task('svg', function() {
  return gulp.src(path.src.svg)
    .pipe(svgMin({
      js2svg: {
        pretty: true
      }
    }))
    .pipe(svgCheerio({
      run: function($) {
        //$('[fill]').removeAttr('fill');
        $('[stroke]').removeAttr('stroke');
        $('[style]').removeAttr('style');
      },
      parseOptions: {xmlMode: true}
    }))
    .pipe(svgReplace('&gt;', '>'))
    .pipe(svgSprite({
      mode: {
        css: {
          spacing: {
            padding: 5
          },
          dest: '.',
          bust: false,
          sprite: 'sprite.svg',
          render: {
            scss: {
              dest: path.src.spriteStyle,
              template: path.src.spriteTemplate
            }
          }
        }
      }
    }))
    .pipe(gulp.dest(path.build.svg));
});


// imagemin
gulp.task('images', function () {
  return gulp.src(path.src.images)
    .pipe(plugins.imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(gulp.dest(path.build.images))
});


// очистка папки build
gulp.task('clean', function() {
  return gulp.src(path.build.root, {read: false})
    .pipe(clean());
});

// webserver
gulp.task('webserver', function () {
  return browserSync.init({
    server: {
      baseDir: path.watch.root
    },
    port: "3000",
    open: false
  });
});



// watch
gulp.task('default', ['webserver'], function(){
  gulp.watch(path.src.json, ['json', 'pug']);
  gulp.watch(path.src.pug, ['pug']);
  gulp.watch(path.src.images, ['images']);
  gulp.watch(path.src.js, ['jslibs', 'js']);
  gulp.watch(path.src.svg, ['svg', 'style']);
  gulp.watch(path.src.fonts, ['fonts']);
  gulp.watch(path.src.sass, ['csslibs', 'style']);
  gulp.watch(path.src.sass, ['style']);
});



// build - для разработки
gulp.task('build', function(cb) {
  runSequence('json', 'pug', 'images', 'js', 'csslibs', 'style', cb);
});

