$(function () {
  "use strict";


  window.georgia = {
    selectors : {
      tabs: document.querySelector('.js-tabs'),
      tabsContents: document.querySelectorAll('.walking__text div'),
    },

    jsTabs : function () {
      const tabsElem = georgia.selectors.tabs;
      const tabsContents = georgia.selectors.tabsContents;

      tabsElem.addEventListener('click', function (event) {
        const target = event.target;
        const elem = target.closest('li');
        let targetTab = target.dataset.tab;

        if (!elem) return;
        for(let tabsContent of tabsContents){
          tabsContent.classList.remove('active');
        }
        document.getElementById(targetTab).classList.add('active');
      });
    },

	jsFotorama: function() {
		$('.js-fotorama').fotorama({
			nav: false,
			loop: true
		});
	},


	jsFotoramaThumbs: function() {
		$('.js-fotorama-thumbs').fotorama({
			nav: 'thumbs',
			thumbheight: 120,
			thumbwidth: 120,
			thumbmargin: 18,
			thumbborderwidth: 4
		});
	},


		init : function () {
			this.jsTabs();
			this.jsFotorama();
			this.jsFotoramaThumbs();
		}
	};


	georgia.init();


});